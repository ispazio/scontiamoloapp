//
//  CategoryCollectionViewCell.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 5.5
        self.clipsToBounds = true
        
    }
    
    func configure(category: AdvertCategoryResponse){
        categoryLabel.text = category.name?.withoutHtmlTags
    }
    
    func highlightCell(category: String){
        switch category {
        case "#TOP - Da non perdere": self.contentView.backgroundColor = UIColor(red: 212/255, green: 24/255, blue: 116/255, alpha: 1)
        case "Amazon": self.contentView.backgroundColor = UIColor(red: 143/255, green: 150/255, blue: 157/255, alpha: 1)
        case "App Store": self.contentView.backgroundColor = UIColor(red: 44/255, green: 207/255, blue: 252/255, alpha: 1)
        case "Apple": self.contentView.backgroundColor = UIColor(red: 81/255, green: 81/255, blue: 81/255, alpha: 1)
        case "Beauty": self.contentView.backgroundColor = UIColor(red: 239/255, green: 132/255, blue: 88/255, alpha: 1)
        case "Casa": self.contentView.backgroundColor = UIColor(red: 248/255, green: 211/255, blue: 51/255, alpha: 1)
        case "Coupon": self.contentView.backgroundColor = UIColor(red: 52/255, green: 200/255, blue: 178/255, alpha: 1)
        case "Tech": self.contentView.backgroundColor = UIColor(red: 29/255, green: 124/255, blue: 233/255, alpha: 1)
        case "中国 Offerte Cina": self.contentView.backgroundColor = UIColor(red: 219/255, green: 41/255, blue: 58/255, alpha: 1)
        default: self.contentView.backgroundColor = UIColor(red: 60/255, green: 188/255, blue: 240/255, alpha: 1)
        }
    }
    
    func removeHighlight(){
        self.contentView.backgroundColor = UIColor(named: "Category")
    }
}

extension String {
    var withoutHtmlTags: String {
    return self.replacingOccurrences(of: "<[^>]+>", with: "", options:
    .regularExpression, range: nil).replacingOccurrences(of: "&[^;]+;", with:
    "", options:.regularExpression, range: nil)
    }
}

//
//  AlertCollectionViewCell.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 14/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit

class AlertCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func configure(alert: PostsResponse){
        titleLabel.text = alert.title?.rendered?.withoutHtmlTags
        descriptionLabel.text = alert.content?.rendered?.withoutHtmlTags
    }
    
}

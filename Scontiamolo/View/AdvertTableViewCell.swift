//
//  AdvertTableViewCell.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Kingfisher

class AdvertTableViewCell: UITableViewCell {

    @IBOutlet weak var advertContainerImageView: UIView!
    @IBOutlet weak var advertImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var shopOfferLabel: UILabel!
    @IBOutlet weak var titleAdvertLabel: UILabel!
    @IBOutlet weak var previousAdverPriceLabel: UILabel!
    @IBOutlet weak var currentAdvertPriceLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var couponView: UIView!
    @IBOutlet weak var couponLabel: UILabel!
    @IBOutlet weak var extraInfoView: UIView!
    @IBOutlet weak var extraInfoLabel: UILabel!
    @IBOutlet weak var clipboardButton: UIButton!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var couponImageView: UIImageView!
    
    @IBOutlet weak var couponViewHeight: NSLayoutConstraint!
    
    var mediaAdvertViewModel = MediaAdvertViewModel()
    
    let disposeBag = DisposeBag()
    
    var advertResponse: AdvertResponse?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mediaAdvertViewModel.mediaAdvertModel.mediaAdvertInfo.subscribe { data in
            if let media = data.element, media.isEmpty == false {
                if let imgUrl = media[0].guid?.rendered {
                    guard let url = URL.init(string: imgUrl) else {
                        return
                    }
                    let resource = ImageResource(downloadURL: url)
                    
                    KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
                        switch result {
                        case .success(let value):
                            //print("Image: \(value.image). Got from: \(value.cacheType)")
                            self.advertImageView.image = value.image
                        case .failure(let error):
                            print("Error retrieving image: \(error)")
                        }
                    }
                }
            }
        }.disposed(by: disposeBag)
        
        graphicalAdjustments()
        
    }
    
    func graphicalAdjustments(){
        containerView.backgroundColor = UIColor(named: "Box")
        containerView.layer.cornerRadius = 20
        containerView.clipsToBounds = true
        
        advertContainerImageView.layer.borderWidth = 1.0
        advertContainerImageView.layer.borderColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0).cgColor
        advertContainerImageView.layer.cornerRadius = 7.5
        advertContainerImageView.clipsToBounds = true
        advertContainerImageView.backgroundColor = .white
        
        couponView.layer.cornerRadius = 5.5
        couponView.clipsToBounds = true
        couponView.backgroundColor = UIColor(named: "Box")
        //ADD DASHED LINE
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = CGColor(srgbRed: 112/255, green: 112/255, blue: 112/255, alpha: 1.0) //UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1.0).cgColor
        yourViewBorder.lineDashPattern = [5, 5]
        yourViewBorder.frame = couponView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: couponView.bounds).cgPath
        couponView.layer.addSublayer(yourViewBorder)
        
        extraInfoView.layer.cornerRadius = 7.5
        extraInfoView.clipsToBounds = true
        extraInfoView.backgroundColor = UIColor(named: "ExtraDescriptionBox")
        
        tagView.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 5)
        tagView.clipsToBounds = true
        
        dateView.layer.cornerRadius = 8
        
        if traitCollection.userInterfaceStyle == .light {
            containerView.layer.masksToBounds = false
            containerView.layer.shadowColor = UIColor(red: 198/255, green: 214/255, blue: 224/255, alpha: 1.0).cgColor
            containerView.layer.shadowOpacity = 0.8
            containerView.layer.shadowOffset = CGSize(width: 1, height: 4)
            containerView.layer.shadowRadius = 6
        }
        
        currentAdvertPriceLabel.textColor = UIColor(named: "BoxFont")
        titleAdvertLabel.textColor = UIColor(named: "BoxFont")
        shopOfferLabel.textColor = UIColor(named: "FontCredit")
        couponLabel.textColor = UIColor(named: "BoxFont")
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        graphicalAdjustments()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(advert: AdvertResponse){
        advertResponse = advert
        if let mediaId = advert.id {
            advertImageView.image = UIImage(named: "advertPlaceholder")
            mediaAdvertViewModel.dataFetch(id: mediaId)
        }else{
            //print("I COULDN'T LOCATE THE IMAGE. SET PLACEHOLDER")
        }
        
        titleAdvertLabel.text = advert.title?.rendered?.withoutHtmlTags
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: advert.prezzo_iniziale ?? "")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        previousAdverPriceLabel.attributedText = attributeString
        currentAdvertPriceLabel.text = advert.prezzo_scontato
        currentAdvertPriceLabel.adjustsFontSizeToFitWidth = true
        couponLabel.text = advert.coupon_code
        if let date = advert.date {
            //print("STRING TO DATE \(stringToDate(string: date))")
            dateLabel.adjustsFontSizeToFitWidth = true
            dateLabel.text = stringToDate(string: date).timeAgoDisplay()
        }
        extraInfoLabel.text = advert.descrizione_breve
        
        if advert.coupon_code == "" {
            self.couponView.isHidden = true
            self.couponViewHeight.constant = 0
        }else{
            self.couponView.isHidden = false
            self.couponViewHeight.constant = 45
        }
        
        if advert.descrizione_breve == "" {
            self.extraInfoView.isHidden = true
        }else{
            self.extraInfoView.isHidden = false
        }
    }
    
    @IBAction func shareAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name( "advertShareTapped"), object: advertResponse!)
    }
    
    @IBAction func clipboardButton(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name( "copyCliboardTapped"), object: advertResponse!)
        couponLabel.text = "Copiato!".localized
        couponImageView.image = UIImage(systemName: "doc.on.doc.fill")
        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
            self.couponLabel.text = self.advertResponse?.coupon_code
        })
    }

}

extension UITableViewCell {
    func stringToDate(string: String)-> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from:string)!
        return date
    }
}

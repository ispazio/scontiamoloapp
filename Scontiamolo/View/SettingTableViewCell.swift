//
//  SettingTableViewCell.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 10/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {

    @IBOutlet weak var settingSwitch: UISwitch!
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var settingImageView: UIImageView!
    @IBOutlet weak var chevronRightImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func switchPushedAction(_ sender: UISwitch) {
        print("SENDER TAPPED \(sender.tag)")
        if sender.tag == 0 {
            NotificationCenter.default.post(name: Notification.Name("changeIcon"), object: settingSwitch.isOn)
        }else if sender.tag == 1 {
            NotificationCenter.default.post(name: Notification.Name("pushNotification"), object: settingSwitch.isOn)
        }else if sender.tag == 2 {
            //NotificationCenter.default.post(name: Notification.Name("switchDark"), object: settingSwitch.isOn)
        }
    }
    
}

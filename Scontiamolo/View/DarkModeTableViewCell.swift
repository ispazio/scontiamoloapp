//
//  DarkModeTableViewCell.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 11/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit

class DarkModeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var darkModeLabel: UILabel!
    @IBOutlet weak var darkModeImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    func setThick(){
        self.accessoryType = .checkmark
    }
    
    func unsetThick(){
        self.accessoryType = .none
    }

}

//
//  PostTableViewCell.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 11/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class PostTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var postContentView: UIView!
    @IBOutlet weak var shareButton: UIButton!
    
    var mediaAdvertViewModel = MediaAdvertViewModel()
       
    let disposeBag = DisposeBag()
    
    var refreshControl: UIRefreshControl!
    
    var postResponse: PostsResponse?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mediaAdvertViewModel.mediaAdvertModel.mediaAdvertInfo.subscribe { data in
            if let media = data.element, media.isEmpty == false {
                if let imgUrl = media[0].media_details?.sizes?.medium_large?.source_url {
                    guard let url = URL.init(string: imgUrl) else {
                        return
                    }
                    let resource = ImageResource(downloadURL: url)
                    
                    KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
                        switch result {
                        case .success(let value):
                            print("Image: \(value.image). Got from: \(value.cacheType)")
                            self.postImageView.image = value.image
                        case .failure(let error):
                            print("Error retrieving image: \(error)")
                        }
                    }
                }
            }
        }.disposed(by: disposeBag)
        
        postContentView.layer.cornerRadius = 15
        postContentView.clipsToBounds = true
        
        postContentView.layer.masksToBounds = false
        postContentView.layer.shadowColor = UIColor.label.cgColor
        postContentView.layer.shadowOpacity = 0.16
        postContentView.layer.shadowOffset = CGSize(width: -1, height: 1)
        postContentView.layer.shadowRadius = 5
        
        postImageView.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 15)
        
        shareButton.layer.masksToBounds = false
        shareButton.layer.shadowColor = UIColor.black.cgColor //UIColor(red: 200/255, green: 216/255, blue: 226/255, alpha: 1.0).cgColor
        shareButton.layer.shadowOpacity = 0.6
        shareButton.layer.shadowOffset = CGSize(width: 1, height: 7)
        shareButton.layer.shadowRadius = 6
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(post: PostsResponse){
        postResponse = post
        if let mediaId = post.id {
            postImageView.image = UIImage(named: "placeholder")
            mediaAdvertViewModel.dataFetch(id: mediaId)
        }else{
            //Setup some placeholder
        }
        titleLabel.text = (post.title?.rendered ?? "").withoutHtmlTags
        if let date = post.date {
            dateLabel.adjustsFontSizeToFitWidth = true
            dateLabel.text = stringToDate(string: date).timeAgoDisplay().uppercased()
        }
        
    }
    
    @IBAction func shareAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name( "postShareTapped"), object: postResponse!)
    }
    

}

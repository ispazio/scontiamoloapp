//
//  ArticlesCollectionViewCell.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 08/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Kingfisher

class ArticlesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var descriptionBoxView: UIView!
    
    var mediaAdvertViewModel = MediaAdvertViewModel()
    
    let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mediaAdvertViewModel.mediaAdvertModel.mediaAdvertInfo.subscribe { data in
            if let media = data.element, media.isEmpty == false {
                if let imgUrl = media[0].media_details?.sizes?.medium_large?.source_url {
                    guard let url = URL.init(string: imgUrl) else {
                        return
                    }
                    let resource = ImageResource(downloadURL: url)
                    
                    KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
                        switch result {
                        case .success(let value):
                            print("Image: \(value.image). Got from: \(value.cacheType)")
                            self.articleImageView.image = value.image
                        case .failure(let error):
                            print("Error retrieving image: \(error)")
                        }
                    }
                }
            }
        }.disposed(by: disposeBag)
        
        graphicalAdjustments()
    }
    
    func graphicalAdjustments(){
        articleImageView.layer.cornerRadius = 10
        articleImageView.clipsToBounds = true
        descriptionBoxView.layer.cornerRadius = 10
        descriptionBoxView.clipsToBounds = true
        
        if traitCollection.userInterfaceStyle == .light {
            descriptionBoxView.layer.masksToBounds = false
            descriptionBoxView.layer.shadowColor = UIColor.black.cgColor //UIColor(red: 200/255, green: 216/255, blue: 226/255, alpha: 1.0).cgColor
            descriptionBoxView.layer.shadowOpacity = 0.15
            descriptionBoxView.layer.shadowOffset = CGSize(width: 1, height: 7)
            descriptionBoxView.layer.shadowRadius = 6
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        graphicalAdjustments()
    }
    
    func configure(posts: PostsResponse){
        if let mediaId = posts.id {
            articleImageView.image = UIImage(named: "placeholder")
            mediaAdvertViewModel.dataFetch(id: mediaId)
        }else{
            //Setup some placeholder
        }
        
        articleTitleLabel.text = (posts.title?.rendered ?? "").withoutHtmlTags
    }
    
}

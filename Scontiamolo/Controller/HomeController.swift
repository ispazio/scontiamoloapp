//
//  ViewController.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit
import SafariServices
import RxCocoa
import RxSwift
import StoreKit
import AVFoundation

class HomeController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UITextFieldDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchTextField: UITextFieldPadding!
    @IBOutlet weak var articlesView: UIView!
    @IBOutlet weak var articlesCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var followUsOnTelegram: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    //Constraints outlet
    @IBOutlet weak var articlesViewHeight: NSLayoutConstraint!
    //@IBOutlet weak var categoryTopToArticles: NSLayoutConstraint!
    @IBOutlet weak var searchbarConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewTopSpaceToCategoryConstraint: NSLayoutConstraint!
    @IBOutlet weak var halfViewHeight: NSLayoutConstraint!
    
    let advertCategoryViewModel = AdvertCategoryViewModel()
    let advertViewModel = AdvertViewModel()
    let postsViewModel = PostsViewModel()
    let pageViewModel = PageViewModel()
    
    let disposeBag = DisposeBag()
    
    var advertCategory = [AdvertCategoryResponse]()
    var advert = [AdvertResponse]()
    var advertCopy = [AdvertResponse]()
    var posts = [PostsResponse]()
    var pages = [PostsResponse]()
    var advertCategoryDic = [String: AdvertCategoryResponse]()
    
    var categoryName = ""
    var categoryId = ""
    
    var refreshControl: UIRefreshControl!
    
    var currentSelectedCell: IndexPath = IndexPath(row: 0, section: 0)
    
    var previousSearch = ""
    var pageCounter = 1
    var isEditingText = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        advertViewModel.advertModel.advertError.subscribe({ state in
            switch state.element {
            case .loading:
                DispatchQueue.main.async {
                    self.errorLabel.isHidden = true
                    self.followUsOnTelegram.isHidden = true
                    self.activityIndicatorView.isHidden = false
                    self.searchTextField.isUserInteractionEnabled = false
                }
            case .success:
                DispatchQueue.main.async {
                    self.activityIndicatorView.isHidden = true
                    self.refreshControl.endRefreshing()
                    self.searchTextField.isUserInteractionEnabled = true
                }
            case .error:
                DispatchQueue.main.async {
                    print("ERROR WHILE DOWNLOADING ADVERT")
                    if self.advert.isEmpty {
                        self.errorLabel.isHidden = false
                        self.followUsOnTelegram.isHidden = false
                        self.errorLabel.text = "Si è verificato un errore."
                        self.activityIndicatorView.isHidden = true
                    }else{
                        self.activityIndicatorView.isHidden = true
                    }
                    self.searchTextField.isUserInteractionEnabled = true
                }
            default: print("DEFAULT")
            }
        }).disposed(by: disposeBag)
        
        advertCategoryViewModel.advertCategoryModel.advertCategoryInfo.subscribe { data in
            if let categoryData = data.element, categoryData.isEmpty == false {
                //print("I OBTAINED THE DATAS FOR THE CATEGORY \(categoryData)")
                self.advertCategory = categoryData
                self.advertCategory.insert(AdvertCategoryResponse(id: nil, count: 0, description: nil, link: nil, name: "Tutti"), at: 0)
                self.categoryCollectionView.reloadData()
                for category in categoryData {
                    self.advertCategoryDic[String((category.id)!)] = category
                }
            }
        }.disposed(by: disposeBag)
        
        advertViewModel.advertModel.advertInfo.subscribe { data in
            if let advert = data.element, advert.isEmpty == false {
                //print("I OBTAINED THE DATA FOR THE ADVERT \(advert)")
                DispatchQueue.main.async {
                    
                    /*if self.pageCounter == 1 {
                        self.advert = advert
                        self.tableView.reloadData()
                    }else{
                        self.advert = self.advertCopy
                        for advert in advert {
                            self.advert.append(advert)
                            self.tableView.beginUpdates()
                            self.tableView.insertRows(at: [IndexPath.init(row: self.advert.count-1, section: 0)], with: .automatic)
                            self.tableView.endUpdates()
                        }
                        self.advertCopy = self.advert
                    }*/
                    
                    
                    for advert in advert {
                        self.advert.append(advert)
                        self.tableView.beginUpdates()
                        self.tableView.insertRows(at: [IndexPath.init(row: self.advert.count-1, section: 0)], with: .automatic)
                        self.tableView.endUpdates()
                    }
                    self.advertCopy = self.advert
                    
                    self.loadNewPage()
                    self.errorLabel.isHidden = true
                    self.followUsOnTelegram.isHidden = true
                }
            }else if let advert = data.element, advert.isEmpty{
                print("ADVERT IS EMPTY", self.advertViewModel.advertModel.advertError.value, self.advert.isEmpty, self.categoryId != "")
                if self.advertViewModel.advertModel.advertError.value == .error && self.advert.isEmpty {
                    self.advert.removeAll()
                    self.tableView.reloadData()
                    self.errorLabel.isHidden = false
                    self.followUsOnTelegram.isHidden = false
                    self.errorLabel.text = "La categoria \(self.categoryName.withoutHtmlTags) è vuota"
                }else if advert.isEmpty && self.categoryId != "" {
                    //self.advert.removeAll()
                    self.tableView.reloadData()
                    self.errorLabel.isHidden = false
                    self.followUsOnTelegram.isHidden = false
                    self.errorLabel.text = "La categoria \(self.categoryName.withoutHtmlTags) è vuota"
                }
            }
        }.disposed(by: disposeBag)
        
        postsViewModel.postsModel.postsInfo.subscribe({ data in
            if let posts = data.element, posts.isEmpty == false {
                self.posts = posts
                self.articlesCollectionView.reloadData()
            }
        }).disposed(by: disposeBag)
        
        pageViewModel.pageModel.pageInfo.subscribe({ data in
            //print("PAGES OBTAINED \(data.element)")
            if let page = data.element, page.isEmpty == false {
                self.pages = page
                self.articlesCollectionView.reloadData()
            }
        }).disposed(by: disposeBag)
        
        advertCategoryViewModel.dataFetch()
        self.pageCounter = 1
        advertViewModel.dataFetch(categoryId: "", pageNumber: "1")
        postsViewModel.dataFetch(perPage: "5")
        pageViewModel.dataFetch(perPage: "")
        
        graphicalAdjustments()
        hideKeyboardWhenTappedAround()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Scorri per aggiornare".localized)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        //print("DARK MODE DEFAULT \(UserDefaults.standard.string(forKey: "switchDark"))")
        if let switchDark = UserDefaults.standard.string(forKey: "switchDark") {
            if switchDark == "dark" {
                UIApplication.shared.windows.forEach { window in
                    window.overrideUserInterfaceStyle = .dark
                }
            }else if switchDark == "light"{
                UIApplication.shared.windows.forEach { window in
                    window.overrideUserInterfaceStyle = .light
                }
            }else{
                UIApplication.shared.windows.forEach { window in
                    window.overrideUserInterfaceStyle = .unspecified
                }
            }
        }else{
            UserDefaults.standard.set("unspecified", forKey: "switchDark")
        }
        
        if !UserDefaults.standard.bool(forKey: "welcomeScreen") {
            let vc = self.storyboard?.instantiateViewController(identifier: "welcomeVc") as! WelcomeController
            present(vc, animated: true, completion: nil)
        }
        
    }
    
    func loadNewPage(){
        print("CALLED LOAD NEW PAGE FUNC")
        self.advertCopy = self.advert
        self.pageCounter = self.pageCounter + 1
        print("ADVERT COPY COUNT \(advertCopy.count)")
        /*if advertCopy.count < 100 {
            //DON'T CALL
            self.advertViewModel.dataFetch(categoryId: self.categoryId, pageNumber: String(self.pageCounter))
        }else{
            self.advertViewModel.dataFetch(categoryId: self.categoryId, pageNumber: String(self.pageCounter))
        }*/
        self.advertViewModel.dataFetch(categoryId: self.categoryId, pageNumber: String(self.pageCounter))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        //Setup Observers for Notifications
        NotificationCenter.default.addObserver(self, selector: #selector(shareTapped(_:)), name: NSNotification.Name(rawValue: "advertShareTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(clipboardTapped(_:)), name: NSNotification.Name(rawValue: "copyCliboardTapped"), object: nil)
        let appdelegate = AppDelegate()
        appdelegate.showReview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        //Remove Observers for Notifications
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "advertShareTapped"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "copyCliboardTapped"), object: nil)
    }
    
    @objc func shareTapped(_ notification: Notification){
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        if let advertNotification = notification.object as? AdvertResponse {
            let vc = UIActivityViewController(activityItems: [advertNotification.title?.rendered?.withoutHtmlTags ?? "", "in offerta a \(advertNotification.prezzo_scontato ?? "")", "ecco il link:", advertNotification.link ?? ""], applicationActivities: [])
            if let popoverController = vc.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            present(vc, animated: true)
        }
    }
    
    @objc func clipboardTapped(_ notification: Notification){
        if let advertNotification = notification.object as? AdvertResponse {
            let pasteboard = UIPasteboard.general
            pasteboard.string = advertNotification.coupon_code
        }
    }
    
    @objc func refresh(_ sender: Any) {
        advertCategoryViewModel.dataFetch()
        self.advert.removeAll()
        self.categoryId = ""
        self.pageCounter = 1
        advertViewModel.dataFetch(categoryId: "", pageNumber: "1")
        postsViewModel.dataFetch(perPage: "5")
        pageViewModel.dataFetch(perPage: "")
    }
    
    func graphicalAdjustments(){
        
        searchTextField.layer.cornerRadius = 24
        searchTextField.clipsToBounds = true
        searchTextField.backgroundColor = UIColor(named: "Box")
        
        let searchImage = UIImageView(frame: CGRect(x: -5, y: 8, width: 25, height: 24))
        
        //Apply shadow only if it's light modeEc
        if traitCollection.userInterfaceStyle == .light {
            //print("Light mode")
            searchTextField.layer.masksToBounds = false
            searchTextField.layer.shadowColor = UIColor(red: 198/255, green: 214/255, blue: 224/255, alpha: 1.0).cgColor
            searchTextField.layer.shadowOpacity = 1
            searchTextField.layer.shadowOffset = CGSize(width: -1, height: 1)
            searchTextField.layer.shadowRadius = 5
            searchImage.tintColor = UIColor(red: 174/255, green: 196/255, blue: 209/255, alpha: 1.0)
        }else{
            searchImage.tintColor = UIColor(named: "FontCredit")
        }
        
        headerView.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 19)
        headerView.clipsToBounds = true
        
        searchImage.image = UIImage(systemName: "magnifyingglass")
        
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        iconContainerView.addSubview(searchImage)
        searchTextField.rightView = iconContainerView
        searchTextField.rightViewMode = .always
        
        tableView.backgroundColor = UIColor(named: "Background")
        headerView.backgroundColor = UIColor(named: "Background")
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        graphicalAdjustments()
    }
    
    //MARK: Table View setup
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if advert[indexPath.row].coupon_code != "" && advert[indexPath.row].descrizione_breve != "" {
            return tableView.rowHeight + (advert[indexPath.row].descrizione_breve?.height(withConstrainedWidth: 284, font: UIFont.italicSystemFont(ofSize: 14)))! - 50
        }else if advert[indexPath.row].coupon_code != "" {
            return tableView.rowHeight - 90
        }else if advert[indexPath.row].descrizione_breve != "" {
            return tableView.rowHeight + (advert[indexPath.row].descrizione_breve?.height(withConstrainedWidth: 284, font: UIFont.italicSystemFont(ofSize: 14)))! - 85
        }
        else{
            return 212
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return advert.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "advertCell", for: indexPath) as! AdvertTableViewCell
        cell.configure(advert: advert[indexPath.row])
        if let advertCategory = advert[indexPath.row].advert_category?[0] {
            cell.shopOfferLabel.text = advertCategoryDic[String(advertCategory)]?.name?.uppercased().withoutHtmlTags
            
            switch cell.shopOfferLabel.text {
            case "#TOP - Da non perdere".uppercased(): cell.tagView.backgroundColor = UIColor(red: 212/255, green: 24/255, blue: 116/255, alpha: 1)
            case "Amazon".uppercased(): cell.tagView.backgroundColor = UIColor(red: 143/255, green: 150/255, blue: 157/255, alpha: 1)
            case "App Store".uppercased(): cell.tagView.backgroundColor = UIColor(red: 44/255, green: 207/255, blue: 252/255, alpha: 1)
            case "Apple".uppercased(): cell.tagView.backgroundColor = UIColor(red: 81/255, green: 81/255, blue: 81/255, alpha: 1)
            case "Beauty".uppercased(): cell.tagView.backgroundColor = UIColor(red: 239/255, green: 132/255, blue: 88/255, alpha: 1)
            case "Casa".uppercased(): cell.tagView.backgroundColor = UIColor(red: 248/255, green: 211/255, blue: 51/255, alpha: 1)
            case "Coupon".uppercased(): cell.tagView.backgroundColor = UIColor(red: 52/255, green: 200/255, blue: 178/255, alpha: 1)
            case "Tech".uppercased(): cell.tagView.backgroundColor = UIColor(red: 29/255, green: 124/255, blue: 233/255, alpha: 1)
            case "中国 Offerte Cina".uppercased(): cell.tagView.backgroundColor = UIColor(red: 219/255, green: 41/255, blue: 58/255, alpha: 1)
            default: cell.tagView.backgroundColor = UIColor(red: 60/255, green: 188/255, blue: 240/255, alpha: 1)
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let link = advert[indexPath.row].link_affiliato {
           //showArticle(link)
            if let appURL = URL(string: link) {
                UIApplication.shared.open(appURL) { success in
                    if success {
                        print("The URL was delivered successfully.")
                    } else {
                        print("The URL failed to open.")
                    }
                }
            } else {
                print("Invalid URL specified.")
            }
        }
    }
    
    //MARK: Collection View setup
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == articlesCollectionView {
            if section == 0 {
                if pages.contains(where: {$0.id == 22033}) {
                    return UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 0)
                }else {
                    return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                }
            }else {
                return UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 0)
            }
        }else{
            return UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 0)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == articlesCollectionView {
            return 2
        }else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollectionView {
            return advertCategory.count
        }else if collectionView == articlesCollectionView {
            if section == 1 {
                return posts.count
            }else{
                //print("I'm in number of items \(pages.contains(where: {$0.id == 22033}))")
                if pages.contains(where: {$0.id == 22033}) {
                    return 1
                }else{
                    return 0
                }
            }
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoryCollectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
            cell.configure(category: advertCategory[indexPath.row])
            //Highlight the cell
            if indexPath.row == currentSelectedCell.row{
                cell.highlightCell(category: advertCategory[indexPath.row].name ?? "")
            }else{
                cell.removeHighlight()
            }
            return cell
        }else if collectionView == articlesCollectionView {
            if indexPath.section == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "articleCell", for: indexPath) as! ArticlesCollectionViewCell
                cell.configure(posts: posts[indexPath.row])
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "alertCell", for: indexPath) as! AlertCollectionViewCell
                for page in pages {
                    if page.id == 22033{
                        cell.configure(alert: page)
                    }
                }
                return cell
            }
        }else{
            return UICollectionViewCell()
        }
    }
    
    var positionOffset: IndexPath?
    let pianoSound = URL(fileURLWithPath: Bundle.main.path(forResource: "ispazioClic", ofType: "mp3")!)
    var audioPlayer = AVAudioPlayer()
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCollectionView {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: pianoSound)
                audioPlayer.play()
            } catch {
                // couldn't load file :(
            }
            self.advert.removeAll()
            if let categoryId = advertCategory[indexPath.row].id {
                self.categoryId = String(categoryId)
                self.pageCounter = 1
                advertViewModel.dataFetch(categoryId: String(categoryId), pageNumber: "1")
                categoryName = advertCategory[indexPath.row].name ?? ""
            }else{
                self.categoryId = ""
                self.pageCounter = 1
                advertViewModel.dataFetch(categoryId: "", pageNumber: "1")
            }
            currentSelectedCell = indexPath
            compressView()
            positionOffset = indexPath
            categoryCollectionView.reloadData {
                self.categoryCollectionView.layoutIfNeeded()
                self.categoryCollectionView.scrollToItem(at: self.positionOffset!, at: .centeredHorizontally, animated: false)
            }
            previousSearch = ""
            searchTextField.text = ""
        }else if collectionView == articlesCollectionView {
            if indexPath.section == 1 {
                //Open safari webview
                if let link = posts[indexPath.row].link{
                    showArticle(link)
                }else{
                    //print("NOTHING TO BE SHOWN")
                }
            }else {
                if let link = pages[indexPath.row].link{
                    showArticle(link)
                }else{
                    //print("NOTHING TO BE SHOWN")
                }
            }
        }
    }
    
    //MARK: Scroll view setup
    var scrollOnce = true
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            if scrollView.contentOffset.y > 0 && scrollOnce{
                //self.categoryTopToArticles.constant = -15
                self.articlesViewHeight.constant = 0
                self.searchbarConstraint.constant = 0
                self.headerHeightConstraint.constant = 120
                self.tableViewTopSpaceToCategoryConstraint.constant = 15
                self.halfViewHeight.constant = 30
                
                UIView.animate(withDuration: 0.5, animations: {
                    print("CALLED ANIMATE > 0")
                    self.articlesView.alpha = 0.0
                    self.searchTextField.alpha = 0.0
                    self.view.layoutIfNeeded()
                })
                scrollOnce = false
            }else if scrollView.contentOffset.y < -30 {
                self.searchTextField.text = previousSearch
                //self.categoryTopToArticles.constant = 15
                self.articlesViewHeight.constant = 280
                self.searchbarConstraint.constant = 33
                self.headerHeightConstraint.constant = 143
                self.tableViewTopSpaceToCategoryConstraint.constant = 5
                self.halfViewHeight.constant = 20
                
                UIView.animate(withDuration: 0.5, animations: {
                    print("CALLED ANIMATE < -30")
                    self.articlesView.alpha = 1.0
                    self.searchTextField.alpha = 1.0
                    self.view.layoutIfNeeded()
                })
                scrollOnce = true
                
                if searchTextField.text != ""{
                    searchTextField.text = ""
                    previousSearch = ""
                    refresh(AnyClass.self)
                }
            }
        }
    }
    
    //MARK: TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        compressView()
        isEditingText = false
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        let filteredAdvert: [AdvertResponse]?
        
        filteredAdvert = advertCopy
        
        if let searchText = textField.text {
            advert = searchText.isEmpty ? filteredAdvert! : (filteredAdvert?.filter({(dataString: AdvertResponse) -> Bool in dataString.title?.rendered?.range(of: searchText, options: .caseInsensitive) != nil }))!
            isEditingText = true
            tableView.reloadData()
        }
    }
    
    func compressView(){
        self.previousSearch = searchTextField.text ?? ""
        self.searchTextField.text = ""
        //self.categoryTopToArticles.constant = -15
        self.articlesViewHeight.constant = 0
        self.searchbarConstraint.constant = 0
        self.headerHeightConstraint.constant = 120
        self.tableViewTopSpaceToCategoryConstraint.constant = 15
        self.halfViewHeight.constant = 30
        
        UIView.animate(withDuration: 0.5, animations: {
            self.articlesView.alpha = 0.0
            self.searchTextField.alpha = 0.0
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func goToArticleListAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "postsVc") as! PostsController
        vc.modalPresentationStyle = .overFullScreen
        show(vc, sender: nil)
    }
    
    @IBAction func followAction(_ sender: Any) {
        showArticle("https://t.me/scontiamolo")
    }
    
    
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        clipsToBounds = true
        layer.cornerRadius = radius
        layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showArticle(_ articleUrl: String) {
        if let url = URL(string: articleUrl) {
            let config = SFSafariViewController.Configuration()
            //config.entersReaderIfAvailable = true

            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    func showAlert(title: String, description: String){
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alert, animated: true)
    }
    
}

extension Date {
    func timeAgoDisplay() -> String {
        let date = Date().addingTimeInterval(self.timeIntervalSinceNow)
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .full
        //Date().addingTimeInterval(120.0 * 60.0)
        return formatter.localizedString(for: date, relativeTo: Date().addingTimeInterval(60.0 * 60.0))
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
        
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
    
    func localizeWithFormat(arguments: CVarArg...) -> String{
       return String(format: self.localized, arguments: arguments)
    }

    var localized: String{
        return Bundle.main.localizedString(forKey: self, value: nil, table: nil)
    }
}

extension UICollectionView {
    func reloadData(completion:@escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
            { _ in completion() }
    }
}

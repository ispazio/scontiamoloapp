//
//  DarkModeController.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 11/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit

class DarkModeController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    var currentSelectedCell: IndexPath = IndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 19)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 50))
        view.backgroundColor = .systemBackground
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 0, width: view.frame.width, height: view.frame.height-10)
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = UIColor.label
        label.text = "Aspetto".localized
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "darkModeCell", for: indexPath) as! DarkModeTableViewCell
        if indexPath.row == 0 {
            if let switchDark = UserDefaults.standard.string(forKey: "switchDark") {
                if switchDark == "light"{
                    cell.setThick()
                }else{
                    cell.unsetThick()
                }
            }
            cell.darkModeLabel.text = "Chiaro".localized
            cell.darkModeImageView.image = UIImage(systemName: "sun.max")
            return cell
        }else if indexPath.row == 1 {
            if let switchDark = UserDefaults.standard.string(forKey: "switchDark") {
                if switchDark == "dark"{
                    cell.setThick()
                }else{
                    cell.unsetThick()
                }
            }
            cell.darkModeLabel.text = "Scuro".localized
            cell.darkModeImageView.image = UIImage(systemName: "moon")
            return cell
        }else if indexPath.row == 2 {
            if let switchDark = UserDefaults.standard.string(forKey: "switchDark") {
                if switchDark == "unspecified"{
                    cell.setThick()
                }else{
                    cell.unsetThick()
                }
            }
            cell.darkModeLabel.text = "Automatico".localized
            cell.darkModeImageView.image = UIImage(systemName: "sunset.fill")
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        
        if indexPath.row == 0 {
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .light
            }
            UserDefaults.standard.set("light", forKey: "switchDark")
        }else if indexPath.row == 1 {
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .dark
            }
            UserDefaults.standard.set("dark", forKey: "switchDark")
        }else{
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .unspecified
            }
            UserDefaults.standard.set("unspecified", forKey: "switchDark")
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
        
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

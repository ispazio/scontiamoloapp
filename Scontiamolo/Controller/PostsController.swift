//
//  PostsController.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 11/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PostsController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchTextField: UITextFieldPadding!
    
    let postsViewModel = PostsViewModel()
    
    let disposeBag = DisposeBag()
    
    var posts = [PostsResponse]()
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postsViewModel.postsModel.postsError.subscribe({ state in
            switch state.element {
            case .loading:
                print("LOADING")
            case .success:
                self.refreshControl.endRefreshing()
            case .error:
                print("ERROR")
            default:
                print("DEFAULT")
            }
        }).disposed(by: disposeBag)

        postsViewModel.postsModel.postsInfo.subscribe({ data in
            if let posts = data.element, posts.isEmpty == false {
                self.posts = posts
                self.tableView.reloadData()
            }
        }).disposed(by: disposeBag)
        
        postsViewModel.dataFetch(perPage: "100")
        graphicalAdjustments()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Scorri per aggiornare".localized)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(shareTapped(_:)), name: NSNotification.Name(rawValue: "postShareTapped"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor(named: "NavigationControllerFont")
    }
    
    @objc func shareTapped(_ notification: Notification){
           let generator = UINotificationFeedbackGenerator()
           generator.notificationOccurred(.success)
           if let advertNotification = notification.object as? PostsResponse {
            let vc = UIActivityViewController(activityItems: ["Leggi: \(advertNotification.title?.rendered ?? "")",advertNotification.link ?? ""], applicationActivities: [])
            if let popoverController = vc.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
               present(vc, animated: true)
           }
       }
    
    @objc func refresh(_ sender: Any) {
        postsViewModel.dataFetch(perPage: "100")
    }
    
    func graphicalAdjustments(){
        searchTextField.layer.cornerRadius = 24
        searchTextField.clipsToBounds = true
        searchTextField.backgroundColor = UIColor(named: "Box")
        
        let searchImage = UIImageView(frame: CGRect(x: -5, y: 8, width: 25, height: 24))
        
        //Apply shadow only if it's light modeEc
        if traitCollection.userInterfaceStyle == .light {
            print("Light mode")
            searchTextField.layer.masksToBounds = false
            searchTextField.layer.shadowColor = UIColor(red: 198/255, green: 214/255, blue: 224/255, alpha: 1.0).cgColor
            searchTextField.layer.shadowOpacity = 1
            searchTextField.layer.shadowOffset = CGSize(width: -1, height: 1)
            searchTextField.layer.shadowRadius = 5
            searchImage.tintColor = UIColor(red: 174/255, green: 196/255, blue: 209/255, alpha: 1.0)
        }else{
            searchImage.tintColor = UIColor.white
        }
        
        headerView.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 19)
        headerView.clipsToBounds = true
        
        searchImage.image = UIImage(systemName: "magnifyingglass")
        
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        iconContainerView.addSubview(searchImage)
        searchTextField.rightView = iconContainerView
        searchTextField.rightViewMode = .always
        
        headerView.backgroundColor = UIColor(named: "Background")
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        graphicalAdjustments()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postCell") as! PostTableViewCell
        cell.configure(post: posts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let link = posts[indexPath.row].link {
            showArticle(link)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        let filteredPosts: [PostsResponse]?
        
        filteredPosts = postsViewModel.postsModel.postsInfo.value
        
        if let searchText = textField.text {
            posts = searchText.isEmpty ? filteredPosts! : (filteredPosts?.filter({(dataString: PostsResponse) -> Bool in dataString.title?.rendered?.range(of: searchText, options: .caseInsensitive) != nil }))!
            
            tableView.reloadData()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}

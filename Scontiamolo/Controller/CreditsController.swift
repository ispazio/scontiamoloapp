//
//  CreditsController.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 11/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit
import MessageUI

class CreditsController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var creditsLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        creditsLabel.setRegualAndBoldText(regualText: "Sviluppatore dell'applicazione ".localized, boldiText: "\nPietro Messineo")
        
        creditsLabel.text?.append("\n \nGrafica (UI) e User Experience (UX): Fabiano Confuorto\n\nIcone:\nEmanuele Esposito".localized)
        
        creditsLabel.textColor = UIColor(named: "FontCredit")
        
        headerView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 19)
    }
   
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func contactAction(_ sender: Any) {
        sendEmail(recipients: "info@pietromessineo.com", body: "Sono interessato allo sviluppo di un app.".localized)
    }
    
    func sendEmail(recipients: String, body: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipients])
            mail.setMessageBody(body, isHTML: true)
            present(mail, animated: true)
        } else {
            // show failure alert
            showAlert(title: "Oh oh...", description: "Si è verificato un errore. Controlla se l'app Mail è installata.".localized)
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension UILabel{
    public func setRegualAndBoldText(regualText: String,
                                       boldiText: String) {

        let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: font.pointSize)]
        let regularString = NSMutableAttributedString(string: regualText)
        let boldiString = NSMutableAttributedString(string: boldiText, attributes:attrs)
        regularString.append(boldiString)
        attributedText = regularString
    }
}

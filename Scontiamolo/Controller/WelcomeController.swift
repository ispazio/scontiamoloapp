//
//  WelcomeController.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 12/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit

class WelcomeController: UIViewController {

    @IBOutlet weak var continueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueButton.layer.cornerRadius = 12
        continueButton.clipsToBounds = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(true, forKey: "welcomeScreen")
    }

    @IBAction func continueAction(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "welcomeScreen")
        self.dismiss(animated: true, completion: nil)
    }
    
}

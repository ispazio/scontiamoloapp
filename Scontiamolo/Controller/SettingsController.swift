//
//  SettingsController.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 10/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import UIKit
import SafariServices
import UserNotifications
import OneSignal

class SettingsController: UIViewController, UITableViewDelegate, UITableViewDataSource, UNUserNotificationCenterDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var ispazioButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(changeIcon(_:)), name: NSNotification.Name(rawValue: "changeIcon"), object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(switchDark(_:)), name: NSNotification.Name(rawValue: "switchDark"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(disablePush(_:)), name: NSNotification.Name(rawValue: "pushNotification"), object: nil)
        
        headerView.roundCorners(corners: [.bottomRight, .bottomLeft], radius: 19)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor(named: "NavigationControllerFont")
    }
    
    @objc func changeIcon(_ notification: Notification){
        print("Change Icon pushed",notification.object as? Bool)
        if let isOn = notification.object as? Bool {
            if isOn {
                UIApplication.shared.setAlternateIconName("AppIcon-2") { error in
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        print("Success!")
                    }
                }
                UserDefaults.standard.set(true, forKey: "switchIcon")
            }else {
                UIApplication.shared.setAlternateIconName(nil) { error in
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        print("Success!")
                    }
                }
                UserDefaults.standard.set(false, forKey: "switchIcon")
            }
        }
    }
    
    @objc func disablePush(_ notification: Notification){
        print("Disable pushed",notification.object as? Bool)
        if let isOn = notification.object as? Bool {
            if isOn {
                OneSignal.setSubscription(true)
                UserDefaults.standard.set(true, forKey: "switchPush")
            }else{
                OneSignal.setSubscription(false)
                UserDefaults.standard.set(false, forKey: "switchPush")
            }
        }
    }
    
    /*@objc func switchDark(_ notification: Notification){
        print("Disable pushed",notification.object as? Bool)
        if let isOn = notification.object as? Bool {
            if isOn {
                UIApplication.shared.windows.forEach { window in
                    window.overrideUserInterfaceStyle = .dark
                }
                UserDefaults.standard.set(true, forKey: "switchDark")
            }else{
                UIApplication.shared.windows.forEach { window in
                    window.overrideUserInterfaceStyle = .light
                }
                UserDefaults.standard.set(false, forKey: "switchDark")
            }
        }
    }*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 50))
        view.backgroundColor = .systemBackground
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 0, width: view.frame.width, height: view.frame.height-10)
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = UIColor.label
        
        if section == 0 {
            label.text = "Aspetto".localized
        }else if section == 1 {
            label.text = "Notifiche Push".localized
        }else if section == 2 {
            label.text = "Utilizzo".localized
        }else if section == 3 {
            label.text = "Contatti".localized
        }
        
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 2
        }else if section == 1 || section == 2 {
            return 1
        }else{
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingTableViewCell
                cell.settingLabel.text = "Icona alternativa".localized
                cell.imageView?.image = UIImage(systemName: "square.stack")
                cell.imageView?.tintColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1.0)
                cell.settingSwitch.tag = 0
                if UserDefaults.standard.bool(forKey: "switchIcon") {
                    cell.settingSwitch.isOn = true
                }else{
                    cell.settingSwitch.isOn = false
                }
                cell.settingSwitch.isHidden = false
                cell.chevronRightImage.isHidden = true
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingTableViewCell
                cell.settingLabel.text = "Dark mode"
                cell.imageView?.image = UIImage(systemName: "lightbulb")
                cell.imageView?.tintColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1.0)
                cell.settingSwitch.tag = 2
                cell.settingSwitch.isHidden = true
                cell.chevronRightImage.isHidden = false
                return cell
            }
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingTableViewCell
            cell.settingLabel.text = "Attive".localized
            cell.imageView?.image = UIImage(systemName: "app.badge")
            cell.imageView?.tintColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1.0)
            cell.settingSwitch.tag = 1
            if UserDefaults.standard.bool(forKey: "switchPush") {
                cell.settingSwitch.isOn = true
            }else{
                cell.settingSwitch.isOn = false
            }
            cell.chevronRightImage.isHidden = true
            cell.settingSwitch.isHidden = false
            return cell
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingTableViewCell
            cell.settingLabel.text = "Svuota la cache".localized
            cell.imageView?.image = UIImage(systemName: "exclamationmark.shield")
            cell.imageView?.tintColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1.0)
            cell.settingSwitch.isHidden = true
            cell.chevronRightImage.isHidden = true
            return cell
        }else if indexPath.section == 3 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingTableViewCell
                cell.settingLabel.text = "Segnala un'offerta".localized
                cell.imageView?.image = UIImage(systemName: "envelope.badge")
                cell.imageView?.tintColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1.0)
                cell.settingSwitch.isHidden = true
                cell.chevronRightImage.isHidden = true
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingTableViewCell
                cell.settingLabel.text = "Pubblicità".localized
                cell.imageView?.image = UIImage(systemName: "cube.box")
                cell.imageView?.tintColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1.0)
                cell.settingSwitch.isHidden = true
                cell.chevronRightImage.isHidden = true
                return cell
            }else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingTableViewCell
                cell.settingLabel.text = "Seguici su Telegram".localized
                cell.imageView?.image = UIImage(named: "TelegramLogo")
                cell.settingSwitch.isHidden = true
                cell.chevronRightImage.isHidden = true
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingTableViewCell
                cell.settingLabel.adjustsFontSizeToFitWidth = true
                cell.settingLabel.text = "Crediti".localized
                cell.imageView?.image = UIImage(systemName: "list.bullet.below.rectangle")
                cell.imageView?.tintColor = UIColor(red: 126/255, green: 126/255, blue: 126/255, alpha: 1.0)
                cell.settingSwitch.isHidden = true
                cell.chevronRightImage.isHidden = false
                return cell
            }
        }else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 1 {
                let vc = self.storyboard?.instantiateViewController(identifier: "darkModeVc") as! DarkModeController
                vc.modalPresentationStyle = .overFullScreen
                show(vc, sender: nil)
            }
        }else if indexPath.section == 1 {
            
        }else if indexPath.section == 2 {
            //Empty cache
            showAlert(title: "Fatto!".localized, description: "La cache è stata vuotata.".localized)
        }else if indexPath.section == 3 {
            if indexPath.row == 0 {
                //Segnala un offerta
                showArticle("https://scontiamolo.com/advertise/")
            }else if indexPath.row == 1 {
                //Pubblicità
                showArticle("https://scontiamolo.com/advertise/")
            }else if indexPath.row == 2 {
                //Seguici su Telegram
                showArticle("https://t.me/scontiamolo")
            }else if indexPath.row == 3 {
                //Credits
                let vc = self.storyboard?.instantiateViewController(identifier: "creditsVc") as! CreditsController
                vc.modalPresentationStyle = .overFullScreen
                show(vc, sender: nil)
            }
        }
    }
    
    @IBAction func legalAction(_ sender: Any) {
        showArticle("https://scontiamolo.com/legal/")
    }
    
    @IBAction func privacyAction(_ sender: Any) {
        showArticle("https://scontiamolo.com/privacy")
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ispazioAction(_ sender: Any) {
        showArticle("https://www.ispazio.net")
    }
    
    
}

//
//  FetchAdvertMedia.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation

class FetchMediaAdvert {
    static let shared = FetchMediaAdvert()
    
    func fetchMediaAdvert(id: Int, completion: @escaping ([MediaAdvertResponse]) -> (), errorState: @escaping (Error?) -> () ) {
        let url = "https://scontiamolo.com/wp-json/wp/v2/media?parent=\(id)"
        //print("FETCH MEDIA ADVERT \(url)")
        let urlObj = URL(string: url)
        var request = URLRequest(url: urlObj!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if let error = error {
                    errorState(error)
                }else {
                    do {
                        let info = try JSONDecoder().decode([MediaAdvertResponse].self, from: data!)
                        DispatchQueue.main.async {
                            completion(info)
                        }
                    } catch {
                        print("ERROR FROM MEDIA ADVERT RESPONSE", error)
                        errorState(error)
                    }
                }
            }
        }.resume()
    }
    
}

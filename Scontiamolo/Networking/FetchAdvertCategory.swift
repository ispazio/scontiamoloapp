//
//  FetchAdvertCategory.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation

class FetchAdvertCategory {
    static let shared = FetchAdvertCategory()
    
    func fetchAdvertCategory(completion: @escaping ([AdvertCategoryResponse]) -> (), errorState: @escaping (Error?) -> () ) {
        let url = "https://scontiamolo.com/wp-json/wp/v2/advert_category?per_page=100"
        print("FETCH ADVERT CATEGORY \(url)")
        let urlObj = URL(string: url)
        var request = URLRequest(url: urlObj!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if let error = error {
                    errorState(error)
                }else {
                    do {
                        let info = try JSONDecoder().decode([AdvertCategoryResponse].self, from: data!)
                        DispatchQueue.main.async {
                            completion(info)
                        }
                    } catch {
                        print("ERROR FROM ADVERT CATEGORY RESPONSE", error)
                        errorState(error)
                    }
                }
            }
        }.resume()
    }
    
}

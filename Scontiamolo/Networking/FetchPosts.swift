//
//  FetchPosts.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 08/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation

class FetchPosts {
    static let shared = FetchPosts()
    
    func fetchPosts(perPage: String, completion: @escaping ([PostsResponse]) -> (), errorState: @escaping (Error?) -> () ) {
        let url = "https://scontiamolo.com/wp-json/wp/v2/posts?per_page=\(perPage)"
        print("FETCH POSTS \(url)")
        let urlObj = URL(string: url)
        var request = URLRequest(url: urlObj!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if let error = error {
                    errorState(error)
                }else {
                    do {
                        let info = try JSONDecoder().decode([PostsResponse].self, from: data!)
                        DispatchQueue.main.async {
                            completion(info)
                        }
                    } catch {
                        print("ERROR FROM POSTS RESPONSE", error)
                        errorState(error)
                    }
                }
            }
        }.resume()
    }
    
}

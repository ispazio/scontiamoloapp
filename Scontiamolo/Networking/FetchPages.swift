//
//  FetchPages.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 14/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation

class FetchPages {
    static let shared = FetchPages()
    
    func fetchPages(perPage: String, completion: @escaping ([PostsResponse]) -> (), errorState: @escaping (Error?) -> () ) {
        let url = "https://scontiamolo.com/wp-json/wp/v2/pages"
        print("FETCH POSTS \(url)")
        let urlObj = URL(string: url)
        var request = URLRequest(url: urlObj!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if let error = error {
                    errorState(error)
                }else {
                    do {
                        let info = try JSONDecoder().decode([PostsResponse].self, from: data!)
                        DispatchQueue.main.async {
                            completion(info)
                        }
                    } catch {
                        print("ERROR FROM POSTS RESPONSE", error)
                        errorState(error)
                    }
                }
            }
        }.resume()
    }
    
}

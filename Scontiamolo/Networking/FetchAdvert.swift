//
//  FetchAdvert.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation

class FetchAdvert {
    static let shared = FetchAdvert()
    
    func fetchAdvert(pageNumber: String, categoryId:String,completion: @escaping ([AdvertResponse]) -> (), errorState: @escaping (Error?) -> () ) {
        let url = "https://scontiamolo.com/wp-json/wp/v2/advert?per_page=100&page=\(pageNumber)\(categoryId != "" ? "&advert_category=\(categoryId)" : "")"
        print("FETCH ALL ADVERT \(url)")
        let urlObj = URL(string: url)
        var request = URLRequest(url: urlObj!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if let error = error {
                    errorState(error)
                }else {
                    do {
                        let info = try JSONDecoder().decode([AdvertResponse].self, from: data!)
                        DispatchQueue.main.async {
                            completion(info)
                        }
                    }catch {
                        print("ERROR FROM ALL ADVERT RESPONSE", error)
                        errorState(error)
                    }
                }
            }
        }.resume()
    }
    
}

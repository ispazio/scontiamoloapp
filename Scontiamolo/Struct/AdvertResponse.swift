//
//  AdvertAllResponse.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation

struct AdvertResponse: Codable {
    let id: Int?
    let link: String?
    let date: String?
    let title: Title?
    let content: Title?
    let featured_media: Int?
    let advert_category: [Int]?
    let prezzo_scontato: String?
    let link_affiliato: String?
    let coupon_code: String?
    let offerta_lampo: String?
    let termina_il: String?
    let prezzo_iniziale: String?
    let ebay: String?
    let descrizione_breve: String?
    
    let code: String?
    let message: String?
}

struct Title: Codable {
    let rendered: String?
}

//
//  MediaAdvertResponse.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation

struct MediaAdvertResponse: Codable {
    let guid: Guid?
    let media_details: Media?
}

struct Guid: Codable {
    let rendered: String?
}

struct Media: Codable {
    let sizes: MediaSizes?
}

struct MediaSizes: Codable {
    let medium: Medium?
    let large: Medium?
    let medium_large: Medium?
}

struct Medium: Codable {
    let source_url: String?
}

//
//  AdvertCategoryResponse.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation

struct AdvertCategoryResponse: Codable {
    let id, count: Int?
    let description: String?
    let link: String?
    let name: String?
}

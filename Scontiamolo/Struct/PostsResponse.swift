//
//  PostsResponse.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 08/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation

struct PostsResponse: Codable {
    let link: String?
    let title: Title?
    let id: Int?
    let date: String?
    let content: Title?
    let featured_media: Int?
}

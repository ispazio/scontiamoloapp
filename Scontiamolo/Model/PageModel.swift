//
//  PostsModel.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 08/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class PageModel {
    var pageInfo: BehaviorRelay<[PostsResponse]> = BehaviorRelay(value: [])
    var pageError: BehaviorRelay<State> = BehaviorRelay(value: .loading)
    
    enum State {
        case loading
        case error
        case success
    }
}

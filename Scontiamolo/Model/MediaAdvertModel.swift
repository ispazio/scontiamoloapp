//
//  MediaAdvertModel.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MediaAdvertModel {
    var mediaAdvertInfo: BehaviorRelay<[MediaAdvertResponse]> = BehaviorRelay(value: [])
    var mediaAdvertError: BehaviorRelay<State> = BehaviorRelay(value: .loading)
    
    enum State {
        case loading
        case error
        case success
    }
}

//
//  PostsViewModel.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 08/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class PageViewModel {
    var pageModel = PageModel()
    
    func dataFetch(perPage: String){
        pageModel.pageInfo.accept([])
        pageModel.pageError.accept(.loading)
        
        FetchPages.shared.fetchPages(perPage: perPage, completion: { data in
            self.pageModel.pageInfo.accept(data)
            self.pageModel.pageError.accept(.success)
        }, errorState: { error in
            self.pageModel.pageError.accept(.error)
        })
        
    }
}

//
//  AdvertViewModel.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AdvertViewModel {
    var advertModel = AdvertModel()
    
    func dataFetch(categoryId: String, pageNumber: String){
        advertModel.advertInfo.accept([])
        advertModel.advertError.accept(.loading)
        
        FetchAdvert.shared.fetchAdvert(pageNumber: pageNumber, categoryId: categoryId, completion: { data in
            self.advertModel.advertInfo.accept(data)
            self.advertModel.advertError.accept(.success)
        }, errorState: { error in
            self.advertModel.advertError.accept(.error)
        })
    }
}

//
//  MediaAdvertViewModel.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MediaAdvertViewModel {
    var mediaAdvertModel = MediaAdvertModel()
    
    func dataFetch(id: Int){
        mediaAdvertModel.mediaAdvertInfo.accept([])
        mediaAdvertModel.mediaAdvertError.accept(.loading)
        
        FetchMediaAdvert.shared.fetchMediaAdvert(id: id, completion: { data in
            self.mediaAdvertModel.mediaAdvertInfo.accept(data)
            self.mediaAdvertModel.mediaAdvertError.accept(.success)
        }, errorState: { error in
            self.mediaAdvertModel.mediaAdvertError.accept(.error)
        })
    }
}

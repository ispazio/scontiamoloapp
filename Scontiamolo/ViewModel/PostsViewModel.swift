//
//  PostsViewModel.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 08/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class PostsViewModel {
    var postsModel = PostsModel()
    
    func dataFetch(perPage: String){
        postsModel.postsInfo.accept([])
        postsModel.postsError.accept(.loading)
        
        FetchPosts.shared.fetchPosts(perPage: perPage,completion: { data in
            self.postsModel.postsInfo.accept(data)
            self.postsModel.postsError.accept(.success)
        }, errorState: { error in
            self.postsModel.postsError.accept(.error)
        })
    }
}

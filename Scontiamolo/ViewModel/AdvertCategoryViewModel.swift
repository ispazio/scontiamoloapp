//
//  AdvertCategoryViewModel.swift
//  Scontiamolo
//
//  Created by Pietro Messineo on 06/04/2020.
//  Copyright © 2020 Pietro Messineo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AdvertCategoryViewModel {
    var advertCategoryModel = AdvertCategoryModel()
    
    func dataFetch(){
        advertCategoryModel.advertCategoryInfo.accept([])
        advertCategoryModel.advertCategoryError.accept(.loading)
        
        FetchAdvertCategory.shared.fetchAdvertCategory(completion: { advertCategoryData in
            self.advertCategoryModel.advertCategoryInfo.accept(advertCategoryData)
            self.advertCategoryModel.advertCategoryError.accept(.success)
        }, errorState: { error in
            self.advertCategoryModel.advertCategoryError.accept(.error)
        })
    }
}
